public class Estudiante{
	//Declaracion atributos
	private String carne;
	private String nombre;
	//Fin declaracion atributos
	
	//Declaracion constructores
	public Estudiante(){} //Fin constructor sin parametros	
	public Estudiante(String carne, String nombre) {this.carne = carne;	this.nombre = nombre;} //Fin constructor con parametros
	//Fin declaracion constructores
	
	//Declaracion metodos
	public String getCarne() {
		return carne;
		
		} //Fin getCarne
	
	public String getNombre() {
		return nombre;
		
		} //Fin getNombre
	//Fin declaracion metodos
	} //Fin clase Estudiante
