import javax.swing.JOptionPane;
public class ControlNotasEstudiantes {
	
	public static void main (String args[]){
		RegistroNotas registro = null;
		RegistroEstudiante estudiantesLista = null;
		Estudiante estudiante = null;
		int opcion = 1;
		String nombre;
		String carne;
		double nota;
		boolean cursoCreado = false;
		boolean estudiantesCreados = false;
		while (opcion != 12){
			opcion = Integer.parseInt(JOptionPane.showInputDialog("1. Crear curso - grupo\n" +
										"2. Registrar estudiantes\n" +
										"3. Lista de clases\n" +
										"4. Registrar nota\n" +
										"5. Mostrar calificacion de un estudiante\n" +
										"6. Modificar nota\n" +
										"7. Mostrar Acta para firmar\n"+
										"8. Mostrar Acta para publicar\n"+
										"9. Mostrar el nombre, el carne y la nota del estudiante más sobresaliente\n"+
										"10. Mostrar el promedio de una evaluacion\n"+
										"11. Mostrar el promedio de notas del grupo\n"+
										"12. Salir"));
		switch (opcion){
			case 1:
				if (cursoCreado == false) {
					int estudiantes;
					int rubros;
					rubros = Integer.parseInt(JOptionPane.showInputDialog("De cuantos rubros estara compuesto el curso?"));
					estudiantes = Integer.parseInt(JOptionPane.showInputDialog("De cuantos estudiantes estara compuesto el curso?"));
					estudiantesLista = new RegistroEstudiante(estudiantes);
					registro = new RegistroNotas(estudiantes, rubros);
					cursoCreado = true;
					} 
				else {JOptionPane.showMessageDialog(null, "El curso ya fue creado");} //Fin condicional if-else

			break;
			
			case 2:
			if (estudiantesCreados == false && cursoCreado == true) {
				for (int indice = 0; indice < estudiantesLista.length(); indice++){
					nombre = JOptionPane.showInputDialog("Ingrese el nombre del estudiante " + (indice + 1));
					carne = JOptionPane.showInputDialog("Ingrese el carne del estudiante " + (indice + 1));
					estudiante = new Estudiante(carne, nombre);
					estudiantesLista.setEstudiante(indice, estudiante);
					estudiantesCreados = true;
					} //Fin bucle for
				} else if (cursoCreado == true) {
					JOptionPane.showMessageDialog(null, "Ya se ha registrado a los estudiantes");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
						} //Fin condicional if-else
			break;
			case 3:
			JOptionPane.showMessageDialog(null, "La lista de la clase es la siguiente:\n" + estudiantesLista.toString());
			
			break;
			case 4:
			if (estudiantesCreados == true && cursoCreado == true) {
				int estudiantes;
				int rubro;
				estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante al que le quiere asignar una nota*\n" + estudiantesLista.toString()));
				estudiantes = estudiantes - 1;
				rubro = Integer.parseInt(JOptionPane.showInputDialog("Seleccione el rubro al que quiere asignar una nota\n" + "Numero de rubros = " + registro.lengthColumna(estudiantes)));
				rubro = rubro - 1;
				nota = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la nota"));
				registro.setNota(estudiantes, rubro, nota);
				JOptionPane.showMessageDialog(null, "Se guardo la nota satisfactoriamente");
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			break;
			case 5:
			if (estudiantesCreados == true && cursoCreado == true){
				int estudiantes;
				int rubro;
				estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante a consultar*\n" + estudiantesLista.toString()));
				estudiantes = estudiantes - 1;
				rubro = Integer.parseInt(JOptionPane.showInputDialog("Seleccione el rubro a consultar\n" + "Numero de rubros = " + registro.lengthColumna(estudiantes)));
				rubro = rubro - 1;
				JOptionPane.showMessageDialog(null, "El estudiante " + estudiantesLista.getEstudiante(estudiantes).getNombre() + " tiene una nota de " + registro.getNota(estudiantes, rubro) + " en el rubro " + rubro);
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			break;
			case 6:
			if (estudiantesCreados == true && cursoCreado == true){
				int estudiantes;
				int rubro;
				estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante con nota a modificar*\n" + estudiantesLista.toString()));
				estudiantes = estudiantes - 1;
				rubro = Integer.parseInt(JOptionPane.showInputDialog("Seleccione el rubro con nota a modificar\n" + "Numero de rubros = " + registro.lengthColumna(estudiantes)));
				rubro = rubro - 1;
				if (registro.getNota(estudiantes, rubro) != 0){
					nota = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la nota\n" + "nota actual: " + registro.getNota(estudiantes, rubro)));
					registro.setNota(estudiantes, rubro, nota);
					JOptionPane.showMessageDialog(null, "Se modifico la nota satisfactoriamente");
					} else {
						JOptionPane.showMessageDialog(null, "No hay ninguna nota en el rubro especificado");
						
						} //Fin condicional if-else anidado
				
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			break;
			case 7:
			int estudiantes;
			estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante del que sacara el acta para firmar*\n" + estudiantesLista.toString()));
			estudiantes = estudiantes - 1;
			JOptionPane.showMessageDialog(null, "El estudiante: " + estudiantesLista.getEstudiante(estudiantes).getNombre() + " carne: " + estudiantesLista.getEstudiante(estudiantes).getCarne() + "obtuvo una nota de " + registro.getPromedioEstudiante(estudiantes));
			break;
			case 8:
			estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante del que sacara el acta para publicar*\n" + estudiantesLista.toString()));
			estudiantes = estudiantes - 1;
			JOptionPane.showMessageDialog(null, "Carne: " + estudiantesLista.getEstudiante(estudiantes).getCarne() + "promedio: " + registro.getPromedioEstudiante(estudiantes));
			
			break;
			case 9:
			if (estudiantesCreados == true && cursoCreado == true){
				int mejorNota[] = registro.posicionarEstudiantesSobresaliente();
				int fila = mejorNota[0];
				int columna = mejorNota[1];
				JOptionPane.showMessageDialog(null, "El estudiante con mejor nota es " + estudiantesLista.getEstudiante(fila).getNombre() + " Carne " + estudiantesLista.getEstudiante(fila).getCarne() + " con una nota de " + registro.getNota(fila, columna));
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			
			break;
			case 10:
			if (estudiantesCreados == true && cursoCreado == true){
				int rubro;
				rubro = Integer.parseInt(JOptionPane.showInputDialog("Seleccione el rubro a consultar" + "Numero de rubros = " + registro.lengthColumna(0)));
				rubro = rubro - 1;
				JOptionPane.showMessageDialog(null, "El promedio del rubro " + (rubro + 1) + " es " + registro.getPromedioEvaluacion(rubro));
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			break;
			case 11:
			if (estudiantesCreados == true && cursoCreado == true){		
				estudiantes = Integer.parseInt(JOptionPane.showInputDialog("*Seleccione el numero del estudiante con nota a modificar*\n" + estudiantesLista.toString()));
				estudiantes = estudiantes - 1;
				JOptionPane.showMessageDialog(null, "El promedio del estudiante es " + registro.getPromedioEstudiante(estudiantes));
				} else if (cursoCreado == false) {
					JOptionPane.showMessageDialog(null, "Aun no se ha creado el curso");
					} else {
						JOptionPane.showMessageDialog(null, "Aun no se han registrado a los estudiantes");
						} //Fin condicional if-else
			break;
			case 12:
			JOptionPane.showMessageDialog(null, "Gracias por usar el acta de notas y estudiantes");
			break;
			default:
			JOptionPane.showMessageDialog(null, "La opcion seleccionada no existe");
			break;
			} //Fin controlador switch
			} //Fin bucle while -- Menu
			System.exit(0);
		} //Fin metodo main
	} //Fin clase ControlNotasEstudiantes
