public class RegistroNotas {
	//Declaracion de atributos 
	private double[][] notas;
	//Fin declaracion de atributos
	
	//Declaracion constructores
	public RegistroNotas(){notas = new double[5][5];} //Fin constructor sin parametros	
	public RegistroNotas(int filas, int columnas) {notas = new double[filas][columnas];} //Fin constructor con parametros
	//Fin declaracion constructores
	
	//Declaracion metodos
	public void setNota(int fila, int columna, double nota) {
		notas[fila][columna] = nota;
		
		} //Fin metodo setNota
	public double getNota(int fila, int columna){
		return notas[fila][columna];
		
		} //Fin metodo getNota
	public int lengthFila() {
		return notas.length;
		} //Fin metodo lengthFila
	public int lengthColumna(int fila) {
		return notas[fila].length;
		} //Fin metodo lengthColumna
	public double getPromedioEstudiante(int estudiante){
		double suma = 0;
		for (int columna = 0; columna < lengthColumna(estudiante); columna++){
			suma += notas[estudiante][columna];
			} //Fin bucle for
		return suma / lengthColumna(estudiante);
		
		} //Fin metodo getPromedioEstudiante
	public double getPromedioEvaluacion(int evaluacion){
		double suma = 0;
		for (int fila = 0; fila < lengthFila(); fila++){
			suma += notas[fila][evaluacion];
			} //Fin bucle for
		return suma / lengthFila();
		
		} //Fin metodo getPromedioEvaluacion
		
	public int[] posicionarEstudiantesSobresaliente(){
		double notaAlta = notas[0][0];
		int [] posicionEstudiante = new int[2];
		for(int filas = 0; filas < lengthFila(); filas ++){
			for (int columnas = 0; columnas < lengthColumna(filas); columnas++){
				if (notaAlta < notas[filas][columnas]){
					posicionEstudiante[0] = filas;
					posicionEstudiante[1] = columnas;
					} //Fin condicional if
				} //Fin bucle for anidado
			} //Fin bucle for
		return posicionEstudiante;
		}
} //Fin clase RegistroNotas
