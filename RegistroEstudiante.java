public class RegistroEstudiante{
	//Declaracion atributos
	private Estudiante[] estudiantes;
	//Fin declaracion atributos
	
	//Declaracion constructores
	public RegistroEstudiante(){estudiantes = new Estudiante[5];} //Fin constructor sin parametros
	public RegistroEstudiante(int indice) {estudiantes = new Estudiante[indice];} //Fin constructor con parametros
	//Fin declaracion constructores
	
	//Declaracion Metodos
	public int length() {
		return estudiantes.length;
		
		} //Fin metodo length
	public void setEstudiante(int indice, Estudiante estudiante){
		estudiantes [indice] = estudiante;
		
		} //Fin metodo setEstudiante
	public Estudiante getEstudiante(int indice){
		return estudiantes [indice];
		
		} //Fin metodo getEstudiante
	public String getCarne(int indice) {
		return estudiantes[indice].getCarne();
		
		} //Fin metodo getCarne
	public String getNombre(int indice){
		return estudiantes[indice].getNombre();
		} //Fin metodo getCarne
		
	public int getIndice (String carne) {
		int indiceEstudiante = 0;
		for (int indice = 0; indice < length(); indice++) {
			if (getCarne(indice).equals(carne)) {
				indiceEstudiante = indice;
				} //Fin condicional if
			} //Fin bucle for
			return indiceEstudiante;
		} //Fin metodo getIndice
		
		
	public String toString(){
		String lista = "";
		for (int indice = 0; indice < length(); indice++){
			lista += "#"+ (indice + 1) +"= Estudiante: " + getNombre(indice) + "\t Carne: " + getCarne(indice) + "\n";
			}
		return lista;
		} //Fin metodo toString
	//Fin declaracion metodos
	} //Fin clase RegistroEstudiante
